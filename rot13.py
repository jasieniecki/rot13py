def rot13c(c):
    if 'a' <= c and c <= 'z':
        c = chr((((ord(c) - ord('a')) + 13) % 26) + ord('a'))
    if 'A' <= c and c <= 'Z':
        c = chr((((ord(c) - ord('A')) + 13) % 26) + ord('A'))
    return c


def rot13(line):
    l = []
    for x in line:
        l.append(x)
    l = [rot13c(x) for x in l]
    return ''.join(l)


input  = open('/dev/stdin', 'r')
output = open('/dev/stdout', 'w')
for line in input:
    line = rot13(line)
    output.write(line)
output.close()
input.close()
